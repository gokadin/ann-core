package node

type Node struct {
	value       float64
	delta       float64
	connections []*connection
}

func NewNode() *Node {
	return &Node{
		connections: make([]*connection, 0),
	}
}

func (n *Node) ConnectTo(nextNode *Node) {
	n.connections = append(n.connections)
}

func (n *Node) Connections() []*connection {
	return n.connections
}

func (n *Node) Connection(index int) *connection {
	return n.connections[index]
}

func (n *Node) ResetValue() {
	n.value = 0.0
}

func (n *Node) ResetDelta() {
	n.delta = 0.0
}

func (n *Node) Value() float64 {
	return n.value
}

func (n *Node) SetValue(value float64) {
	n.value = value
}

func (n *Node) AddValue(value float64) {
	n.value += value
}

func (n *Node) AddDelta(delta float64) {
	n.delta += delta
}

func (n *Node) Delta() float64 {
    return n.delta
}

func (n *Node) Activate(activationFunction func(x float64) float64) {
	n.value = activationFunction(n.value)

	for _, connection := range n.connections {
		connection.nextNode.AddValue(n.value * connection.weight)
	}
}
