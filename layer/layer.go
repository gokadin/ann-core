package layer

import (
	"github.com/gokadin/ann-core/node"
	"log"
)

type Layer struct {
	nodes     []*node.Node
	nextLayer *Layer
    activationFunction func(x float64) float64
	activationFunctionDerivative func(x float64) float64
}

func NewLayer(size int, activationFunctionName string) *Layer {
	nodes := make([]*node.Node, size)
	for i := range nodes {
		nodes[i] = node.NewNode()
	}

	return &Layer{
		nodes: nodes,
        activationFunction: getActivationFunction(activationFunctionName),
		activationFunctionDerivative: getActivationFunctionDerivative(activationFunctionName),
	}
}

func (l *Layer) Size() int {
	return len(l.nodes)
}

func (l *Layer) ConnectTo(nextLayer *Layer) {
	l.nextLayer = nextLayer

	for _, n := range l.nodes {
		for _, nextNode := range nextLayer.nodes {
			n.ConnectTo(nextNode)
		}
	}
}

func (l *Layer) Nodes() []*node.Node {
	return l.nodes
}

func (l *Layer) Values() []float64 {
	values := make([]float64, l.Size())
	for i, n := range l.nodes {
		values[i] = n.Value()
	}

	return values
}

func (l *Layer) SetValues(values []float64) {
	if len(values) != l.Size() {
		log.Fatal("Cannot set values, size mismatch:", len(values), "!=", l.Size())
	}

	for i, value := range values {
		l.nodes[i].SetValue(value)
	}
}

func (l *Layer) ResetValues() {
	for _, n := range l.nodes {
		n.ResetValue()
	}

	if l.nextLayer != nil {
		l.nextLayer.ResetValues()
	}
}

func (l *Layer) ResetDeltas() {
	for _, n := range l.nodes {
		n.ResetDelta()
	}

	if l.nextLayer != nil {
		l.nextLayer.ResetDeltas()
	}
}

func (l *Layer) Activate() {
	for _, n := range l.nodes {
		n.Activate(l.activationFunction)
	}

	if l.nextLayer != nil {
		l.nextLayer.Activate()
	}
}

func (l *Layer) ActivationDerivative() func (x float64) float64 {
	return l.activationFunctionDerivative
}
