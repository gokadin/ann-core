package core

import (
	"github.com/gokadin/ann-core/layer"
	"log"
)

type builder struct {
	network *Network
}

func newBuilder(network *Network) *builder {
	return &builder{
		network: network,
	}
}

func (b *builder) addInputLayer(size int) {
	layers := b.network.getLayers()
	if len(layers) != 0 {
		log.Fatal("You cannot add an input layer after adding other layers.")
	}

	layers = append(layers, layer.NewLayer(size, layer.Identity))
}

func (b *builder) AddHiddenLayer(size int, activationFunction func(x float64) float64) *builder {
	layers := b.network.getLayers()
	if len(layers) == 0 {
		log.Fatal("You must add an input layer before adding a hidden layer.")
	}

	b.addLayer(layers, layer.NewLayer(size, activationFunction))

	return b
}

func (b *builder) AddOutputLayer(size int) *builder {
	layers := b.network.getLayers()
	if len(layers) < 2 {
		log.Fatal("You must add an input layer and at least one hidden layer before adding an output layer.")
	}

	b.addLayer(layers, layer.NewLayer(size, layer.Identity))

	return b
}

func (b *builder) addLayer(layers []*layer.Layer, l *layer.Layer) {
	layers[len(layers)-1].ConnectTo(l)
	layers = append(layers, l)
}
