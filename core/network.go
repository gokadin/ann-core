package core

import (
	"github.com/gokadin/ann-core/layer"
	"log"
)

type Network struct {
	builder *builder
	layers  []*layer.Layer
}

func NewNetwork() *Network {
	n := &Network{
		layers: make([]*layer.Layer, 0),
	}

	n.builder = newBuilder(n)

	return n
}

func (n *Network) AddInputLayer(size int) *builder {
	n.builder.addInputLayer(size)
	return n.builder
}

func (n *Network) getLayers() []*layer.Layer {
	return n.layers
}

func (n *Network) Run(inputs [][]float64) {
	if len(n.layers) < 3 {
		log.Fatal("You must build your layers before running the network.")
	}

	// ...
}
